/*globals $, alert */

$(function () {
    "use strict";
    $(".page").hide();
    $(location.hash).show();
});

$(window).on('hashchange', function () {
    "use strict";
    $(".page").hide();
    $(location.hash).show();
});

$(".option").click(function () {
    "use strict";
    location.hash = $(this).attr("hash");
});